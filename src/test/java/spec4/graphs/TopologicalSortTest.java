package spec4.graphs;

import org.junit.Before;
import org.junit.Test;
import spec4.datastarctures.Graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TopologicalSortTest {
    private List<Graph.Vertex<Integer>> verticies;
    private List<Graph.Edge<Integer>> edges = new ArrayList<>();
    private List<Graph.Vertex<Integer>> expectedResult;

    @Before
    public void init() throws Exception {
        final Graph.Vertex<Integer> cv1 = new Graph.Vertex<>(1);
        final Graph.Vertex<Integer> cv2 = new Graph.Vertex<>(2);
        final Graph.Vertex<Integer> cv3 = new Graph.Vertex<>(3);
        final Graph.Vertex<Integer> cv4 = new Graph.Vertex<>(4);
        final Graph.Vertex<Integer> cv5 = new Graph.Vertex<>(5);
        final Graph.Vertex<Integer> cv6 = new Graph.Vertex<>(6);
        verticies = Arrays.asList(cv1, cv2, cv3, cv4, cv5, cv6);

        final Graph.Edge<Integer> ce1_2 = new Graph.Edge<>(1, cv1, cv2);
        final Graph.Edge<Integer> ce2_4 = new Graph.Edge<>(2, cv2, cv4);
        final Graph.Edge<Integer> ce4_3 = new Graph.Edge<>(3, cv4, cv3);
        final Graph.Edge<Integer> ce3_6 = new Graph.Edge<>(4, cv3, cv6);
        final Graph.Edge<Integer> ce5_6 = new Graph.Edge<>(5, cv5, cv6);
        final Graph.Edge<Integer> ce4_5 = new Graph.Edge<>(6, cv4, cv5);
        edges = Arrays.asList(ce1_2, ce2_4, ce4_3, ce3_6, ce5_6, ce4_5);

        expectedResult = Arrays.asList(cv6, cv3, cv5, cv4, cv2, cv1);
    }

    @Test
    public void topologicalSortOnDirectedGraph() {
        final Graph<Integer> digraph = new Graph<>(Graph.TYPE.DIRECTED, verticies, edges);
        System.out.println("input data " + digraph.getVertices());
        final List<Graph.Vertex<Integer>> results = TopologicalSort.sort(digraph);
        assertTrue("Topological sort error. results=" + results, results.size() != 0);
        System.out.println("result " + results);
        System.out.println("expectedResult " + expectedResult);
        compareResult(results, expectedResult);
    }

    private void compareResult(List<Graph.Vertex<Integer>> results1, List<Graph.Vertex<Integer>> expectedResult) {
        final Iterator<Graph.Vertex<Integer>> iter1 = results1.iterator();
        final Iterator<Graph.Vertex<Integer>> iter2 = expectedResult.iterator();
        assertTrue("Topological sort error. size1=" + results1.size() + " size2=" + expectedResult.size(),
                results1.size() == expectedResult.size());
        while (iter1.hasNext() && iter2.hasNext()) {
            final Graph.Vertex<Integer> v1 = iter1.next();
            final Graph.Vertex<Integer> v2 = iter2.next();
            assertTrue("Topological sort error. v1=" + v1 + " v2=" + v2, v1.equals(v2));
        }
    }

}