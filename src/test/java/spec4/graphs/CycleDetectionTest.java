package spec4.graphs;

import org.junit.Test;
import spec4.datastarctures.Graph;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CycleDetectionTest {

    @Test
    public void cycleCheckOnUndirected() {
        final List<Graph.Vertex<Integer>> cycledVerticies = new ArrayList<>();
        // c циклом
        final Graph.Vertex<Integer> cv1 = new Graph.Vertex<>(1);
        cycledVerticies.add(cv1);
        final Graph.Vertex<Integer> cv2 = new Graph.Vertex<>(2);
        cycledVerticies.add(cv2);
        final Graph.Vertex<Integer> cv3 = new Graph.Vertex<>(3);
        cycledVerticies.add(cv3);
        final Graph.Vertex<Integer> cv4 = new Graph.Vertex<>(4);
        cycledVerticies.add(cv4);
        final Graph.Vertex<Integer> cv5 = new Graph.Vertex<>(5);
        cycledVerticies.add(cv5);
        final Graph.Vertex<Integer> cv6 = new Graph.Vertex<>(6);
        cycledVerticies.add(cv6);
        final Graph.Vertex<Integer> cv7 = new Graph.Vertex<>(7);
        cycledVerticies.add(cv7);

        final List<Graph.Edge<Integer>> cycledEdges = new ArrayList<>();
        final Graph.Edge<Integer> ce1_2 = new Graph.Edge<>(7, cv1, cv2);
        cycledEdges.add(ce1_2);
        final Graph.Edge<Integer> ce2_4 = new Graph.Edge<>(15, cv2, cv4);
        cycledEdges.add(ce2_4);
        final Graph.Edge<Integer> ce3_4 = new Graph.Edge<>(11, cv3, cv4);
        cycledEdges.add(ce3_4);
        final Graph.Edge<Integer> ce3_6 = new Graph.Edge<>(2, cv3, cv6);
        cycledEdges.add(ce3_6);
        final Graph.Edge<Integer> ce5_6 = new Graph.Edge<>(9, cv5, cv6);
        cycledEdges.add(ce5_6);
        final Graph.Edge<Integer> ce4_5 = new Graph.Edge<>(6, cv4, cv5);
        cycledEdges.add(ce4_5);
        final Graph.Edge<Integer> ce6_7 = new Graph.Edge<>(6, cv6, cv7);
        cycledEdges.add(ce6_7);

        final Graph<Integer> undirectedWithCycle = new Graph<>(cycledVerticies, cycledEdges);

        boolean result = CycleDetection.detect(undirectedWithCycle);
        System.out.println(undirectedWithCycle.getEdges());
        System.out.println("граф имеет цикл " + result);
        assertTrue("Cycle detection error.", result);

//        без цикла
        final List<Graph.Vertex<Integer>> verticies = new ArrayList<>();
        final Graph.Vertex<Integer> v1 = new Graph.Vertex<>(1);
        verticies.add(v1);
        final Graph.Vertex<Integer> v2 = new Graph.Vertex<>(2);
        verticies.add(v2);
        final Graph.Vertex<Integer> v3 = new Graph.Vertex<>(3);
        verticies.add(v3);
        final Graph.Vertex<Integer> v4 = new Graph.Vertex<>(4);
        verticies.add(v4);
        final Graph.Vertex<Integer> v5 = new Graph.Vertex<>(5);
        verticies.add(v5);
        final Graph.Vertex<Integer> v6 = new Graph.Vertex<>(6);
        verticies.add(v6);

        final List<Graph.Edge<Integer>> edges = new ArrayList<>();
        final Graph.Edge<Integer> e1_2 = new Graph.Edge<>(7, v1, v2);
        edges.add(e1_2);
        final Graph.Edge<Integer> e2_4 = new Graph.Edge<>(15, v2, v4);
        edges.add(e2_4);
        final Graph.Edge<Integer> e3_4 = new Graph.Edge<>(11, v3, v4);
        edges.add(e3_4);
        final Graph.Edge<Integer> e3_6 = new Graph.Edge<>(2, v3, v6);
        edges.add(e3_6);
        final Graph.Edge<Integer> e4_5 = new Graph.Edge<>(6, v4, v5);
        edges.add(e4_5);

        final Graph<Integer> undirectedWithoutCycle = new Graph<>(verticies, edges);

        result = CycleDetection.detect(undirectedWithoutCycle);
        System.out.println(undirectedWithoutCycle.getEdges());
        System.out.println("граф имеет цикл " + result);
        assertFalse("Cycle detection error.", result);

    }

}