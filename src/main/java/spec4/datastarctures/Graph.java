package spec4.datastarctures;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

import java.util.*;

@Data
@ToString
@EqualsAndHashCode
public class Graph<T extends Comparable<T>> {

    private List<Vertex<T>> vertices = new ArrayList<>();
    private List<Edge<T>> edges = new ArrayList<>();
    private TYPE type = TYPE.UNDIRECTED;

    public Graph(TYPE type) {
        this.type = type;
    }

    /**
     * Deep copies
     **/
    public Graph(Graph<T> g) {
        type = g.getType();
        // Copy the vertices which also copies the edges
        g.getVertices().forEach(v -> this.vertices.add(new Vertex<>(v)));
        this.getVertices().forEach(v -> this.edges.addAll(v.getEdges()));
    }

    public Graph(Collection<Vertex<T>> vertices, Collection<Edge<T>> edges) {
        this(TYPE.UNDIRECTED, vertices, edges);
    }

    public Graph(TYPE type, Collection<Vertex<T>> vertices, Collection<Edge<T>> edges) {
        this(type);

        this.vertices.addAll(vertices);
        this.edges.addAll(edges);

        for (Edge<T> e : edges) {
            final Vertex<T> from = e.from;
            final Vertex<T> to = e.to;

            if (!this.vertices.contains(from) || !this.vertices.contains(to)) { continue; }

            from.addEdge(e);
            if (this.type == TYPE.UNDIRECTED) {
                Edge<T> reciprical = new Edge<>(e.cost, to, from);
                to.addEdge(reciprical);
                this.edges.add(reciprical);
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object g1) {
        if (!(g1 instanceof Graph)) { return false; }

        final Graph<T> g = (Graph<T>) g1;

        final boolean typeEquals = this.type == g.type;
        if (!typeEquals) { return false; }

        final boolean verticesSizeEquals = this.vertices.size() == g.vertices.size();
        if (!verticesSizeEquals) { return false; }

        final boolean edgesSizeEquals = this.edges.size() == g.edges.size();
        if (!edgesSizeEquals) { return false; }

        final Object[] ov1 = this.vertices.toArray();
        Arrays.sort(ov1);
        final Object[] ov2 = g.vertices.toArray();
        Arrays.sort(ov2);
        for (int i = 0; i < ov1.length; i++) {
            final Vertex<T> v1 = (Vertex<T>) ov1[i];
            final Vertex<T> v2 = (Vertex<T>) ov2[i];
            if (!v1.equals(v2)) { return false; }
        }

        final Object[] oe1 = this.edges.toArray();
        Arrays.sort(oe1);
        final Object[] oe2 = g.edges.toArray();
        Arrays.sort(oe2);
        for (int i = 0; i < oe1.length; i++) {
            final Edge<T> e1 = (Edge<T>) oe1[i];
            final Edge<T> e2 = (Edge<T>) oe2[i];
            if (!e1.equals(e2)) { return false; }
        }

        return true;
    }

    public enum TYPE {
        DIRECTED, UNDIRECTED
    }

    @ToString
    @EqualsAndHashCode
    @Data
    public static class Vertex<T extends Comparable<T>> implements Comparable<Vertex<T>> {

        private T value = null;
        private int weight = 0;
        private List<Edge<T>> edges = new ArrayList<>();

        public Vertex(T value) {
            this.value = value;
        }

        public Vertex(T value, int weight) {
            this(value);
            this.weight = weight;
        }

        public Vertex(Vertex<T> vertex) {
            this(vertex.value, vertex.weight);
            this.edges.addAll(vertex.edges);
        }

        public void addEdge(Edge<T> e) {
            edges.add(e);
        }

        @SuppressWarnings("unchecked")
        @Override
        public boolean equals(Object v1) {
            if (!(v1 instanceof Vertex)) { return false; }

            final Vertex<T> v = (Vertex<T>) v1;

            final boolean weightEquals = this.weight == v.weight;
            if (!weightEquals) { return false; }

            final boolean edgesSizeEquals = this.edges.size() == v.edges.size();
            if (!edgesSizeEquals) { return false; }

            final boolean valuesEquals = this.value.equals(v.value);
            if (!valuesEquals) { return false; }

            final Iterator<Edge<T>> iter1 = this.edges.iterator();
            final Iterator<Edge<T>> iter2 = v.edges.iterator();
            while (iter1.hasNext() && iter2.hasNext()) {
                // Only checking the cost
                final Edge<T> e1 = iter1.next();
                final Edge<T> e2 = iter2.next();
                if (e1.cost != e2.cost) { return false; }
            }

            return true;
        }

        @Override
        public int compareTo(Vertex<T> v) {
            final int valueComp = this.value.compareTo(v.value);
            if (valueComp != 0) { return valueComp; }

            if (this.weight < v.weight) { return -1; }
            if (this.weight > v.weight) { return 1; }

            if (this.edges.size() < v.edges.size()) { return -1; }
            if (this.edges.size() > v.edges.size()) { return 1; }

            final Iterator<Edge<T>> iter1 = this.edges.iterator();
            final Iterator<Edge<T>> iter2 = v.edges.iterator();
            while (iter1.hasNext() && iter2.hasNext()) {
                // Only checking the cost
                final Edge<T> e1 = iter1.next();
                final Edge<T> e2 = iter2.next();
                if (e1.cost < e2.cost) { return -1; }
                if (e1.cost > e2.cost) { return 1; }
            }
            return 0;
        }
    }

    @EqualsAndHashCode
    @Data
    public static class Edge<T extends Comparable<T>> implements Comparable<Edge<T>> {

        private Vertex<T> from = null;
        private Vertex<T> to = null;
        private int cost = 0;

        public Edge(int cost, @NonNull Vertex<T> from, @NonNull Vertex<T> to) {
            this.cost = cost;
            this.from = from;
            this.to = to;
        }

        @Override
        public int compareTo(Edge<T> e) {
            if (this.cost < e.cost) { return -1; }
            if (this.cost > e.cost) { return 1; }

            final int from = this.from.compareTo(e.from);
            if (from != 0) { return from; }

            final int to = this.to.compareTo(e.to);
            if (to != 0) { return to; }

            return 0;
        }

        @Override
        public String toString() {
            return "[ " + from.value + "(" + from.weight + ") " + "]" + " -> " +
                    "[ " + to.value + "(" + to.weight + ") " + "]" + " = " + cost + "\n";
        }
    }
}