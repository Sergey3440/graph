package spec4.graphs;

import lombok.NonNull;
import spec4.datastarctures.Graph;

import java.util.ArrayList;
import java.util.List;

public abstract class TopologicalSort {

    private TopologicalSort() { }

    public static List<Graph.Vertex<Integer>> sort(@NonNull Graph<Integer> graph) {
        if (graph.getType() != Graph.TYPE.DIRECTED) {
            throw new IllegalArgumentException("Cannot perform a topological sort on a non-directed graph. graph type = " + graph.getType());
        }

        final Graph<Integer> clone = new Graph<>(graph);
        final List<Graph.Vertex<Integer>> sorted = new ArrayList<>();
        final List<Graph.Vertex<Integer>> noOutgoing = new ArrayList<>();

        final List<Graph.Edge<Integer>> edges = new ArrayList<>();
        edges.addAll(clone.getEdges());

        clone.getVertices().stream().filter(v -> v.getEdges().size() == 0).forEach(noOutgoing::add);

        while (noOutgoing.size() > 0) {
            final Graph.Vertex<Integer> current = noOutgoing.remove(0);
            sorted.add(current);

            int i = 0;
            while (i < edges.size()) {
                final Graph.Edge<Integer> e = edges.get(i);
                final Graph.Vertex<Integer> from = e.getFrom();
                final Graph.Vertex<Integer> to = e.getTo();
                if (to.equals(current)) {
                    edges.remove(e);
                    from.getEdges().remove(e);
                } else {
                    i++;
                }
                if (from.getEdges().size() == 0) { noOutgoing.add(from); }
            }
        }
        // If we have processed all connected vertices and there are edges remaining, graph has multiple connected components.
        if (edges.size() > 0) { return null; }
        return sorted;
    }
}
