package spec4.graphs;


import lombok.NonNull;
import spec4.datastarctures.Graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public abstract class CycleDetection {

    private CycleDetection() { }

    public static boolean detect(@NonNull Graph<Integer> graph) {
        if (graph.getType() != Graph.TYPE.UNDIRECTED) {
            throw new IllegalArgumentException("Graph is needs to be Undirected.");
        }

        final Set<Graph.Vertex<Integer>> visitedVerticies = new HashSet<>();
        final Set<Graph.Edge<Integer>> visitedEdges = new HashSet<>();

        final List<Graph.Vertex<Integer>> verticies = graph.getVertices();
        if (verticies == null || verticies.size() == 0) { return false; }

        final Graph.Vertex<Integer> root = verticies.get(0);
        ArrayList<Graph.Vertex<Integer>> cycle = new ArrayList<>();
        boolean b = depthFirstSearch(root, visitedVerticies, visitedEdges, cycle);
        return b;
    }


    private static boolean depthFirstSearch(Graph.Vertex<Integer> vertex, Set<Graph.Vertex<Integer>> visitedVertices,
                                            Set<Graph.Edge<Integer>> visitedEdges, List<Graph.Vertex<Integer>> cycle) {
        if (!visitedVertices.contains(vertex)) {
            visitedVertices.add(vertex);
            cycle.add(vertex);
            final List<Graph.Edge<Integer>> edges = vertex.getEdges();
            if (edges != null) {
                for (Graph.Edge<Integer> edge : edges) {
                    final Graph.Vertex<Integer> to = edge.getTo();
                    boolean result = false;
                    if (to != null && !visitedEdges.contains(edge)) {
                        visitedEdges.add(edge);
                        final Graph.Edge<Integer> recip = new Graph.Edge<>(edge.getCost(), edge.getTo(), edge.getFrom());
                        visitedEdges.add(recip);
                        result = depthFirstSearch(to, visitedVertices, visitedEdges, cycle);
                    }
                    if (result) {
                        return true;
                    }
                }
            }
        } else {
            // visited
            List<Graph.Vertex<Integer>> cycleTmp = cycle.subList(cycle.indexOf(vertex), cycle.size());
            System.out.println("цикл: " + String.join("->", cycleTmp.stream().map(it -> it.getValue().toString()).collect(Collectors.toList())));
            return true;
        }
        return false;
    }
}
