package spec4;

import spec4.datastarctures.Graph;
import spec4.graphs.CycleDetection;
import spec4.graphs.TopologicalSort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Main {

    private static List<Graph.Vertex<Integer>> verticies;
    private static List<Graph.Edge<Integer>> edges = new ArrayList<>();
    private static List<Graph.Vertex<Integer>> expectedResult;

    public static void main(String[] args) throws Exception {
        cycleCheckOnUndirected();
        init();
        topologicalSortOnDirectedGraph();
    }

    public static void cycleCheckOnUndirected() {
        final List<Graph.Vertex<Integer>> cycledVerticies = new ArrayList<>();
        // c циклом
        final Graph.Vertex<Integer> cv1 = new Graph.Vertex<>(1);
        cycledVerticies.add(cv1);
        final Graph.Vertex<Integer> cv2 = new Graph.Vertex<>(2);
        cycledVerticies.add(cv2);
        final Graph.Vertex<Integer> cv3 = new Graph.Vertex<>(3);
        cycledVerticies.add(cv3);
        final Graph.Vertex<Integer> cv4 = new Graph.Vertex<>(4);
        cycledVerticies.add(cv4);
        final Graph.Vertex<Integer> cv5 = new Graph.Vertex<>(5);
        cycledVerticies.add(cv5);
        final Graph.Vertex<Integer> cv6 = new Graph.Vertex<>(6);
        cycledVerticies.add(cv6);
        final Graph.Vertex<Integer> cv7 = new Graph.Vertex<>(7);
        cycledVerticies.add(cv7);

        final List<Graph.Edge<Integer>> cycledEdges = new ArrayList<>();
        final Graph.Edge<Integer> ce1_2 = new Graph.Edge<>(7, cv1, cv2);
        cycledEdges.add(ce1_2);
        final Graph.Edge<Integer> ce2_4 = new Graph.Edge<>(15, cv2, cv4);
        cycledEdges.add(ce2_4);
        final Graph.Edge<Integer> ce3_4 = new Graph.Edge<>(11, cv3, cv4);
        cycledEdges.add(ce3_4);
        final Graph.Edge<Integer> ce3_6 = new Graph.Edge<>(2, cv3, cv6);
        cycledEdges.add(ce3_6);
        final Graph.Edge<Integer> ce5_6 = new Graph.Edge<>(9, cv5, cv6);
        cycledEdges.add(ce5_6);
        final Graph.Edge<Integer> ce4_5 = new Graph.Edge<>(6, cv4, cv5);
        cycledEdges.add(ce4_5);
        final Graph.Edge<Integer> ce6_7 = new Graph.Edge<>(6, cv6, cv7);
        cycledEdges.add(ce6_7);

        final Graph<Integer> undirectedWithCycle = new Graph<>(cycledVerticies, cycledEdges);

        boolean result = CycleDetection.detect(undirectedWithCycle);
        System.out.println(undirectedWithCycle.getEdges());
        System.out.println("граф имеет цикл " + result);
//        assertTrue("Cycle detection error.", result);
        assert (result);

//        без цикла
        final List<Graph.Vertex<Integer>> verticies = new ArrayList<>();
        final Graph.Vertex<Integer> v1 = new Graph.Vertex<>(1);
        verticies.add(v1);
        final Graph.Vertex<Integer> v2 = new Graph.Vertex<>(2);
        verticies.add(v2);
        final Graph.Vertex<Integer> v3 = new Graph.Vertex<>(3);
        verticies.add(v3);
        final Graph.Vertex<Integer> v4 = new Graph.Vertex<>(4);
        verticies.add(v4);
        final Graph.Vertex<Integer> v5 = new Graph.Vertex<>(5);
        verticies.add(v5);
        final Graph.Vertex<Integer> v6 = new Graph.Vertex<>(6);
        verticies.add(v6);

        final List<Graph.Edge<Integer>> edges = new ArrayList<>();
        final Graph.Edge<Integer> e1_2 = new Graph.Edge<>(7, v1, v2);
        edges.add(e1_2);
        final Graph.Edge<Integer> e2_4 = new Graph.Edge<>(15, v2, v4);
        edges.add(e2_4);
        final Graph.Edge<Integer> e3_4 = new Graph.Edge<>(11, v3, v4);
        edges.add(e3_4);
        final Graph.Edge<Integer> e3_6 = new Graph.Edge<>(2, v3, v6);
        edges.add(e3_6);
        final Graph.Edge<Integer> e4_5 = new Graph.Edge<>(6, v4, v5);
        edges.add(e4_5);

        final Graph<Integer> undirectedWithoutCycle = new Graph<>(verticies, edges);

        result = CycleDetection.detect(undirectedWithoutCycle);
        System.out.println(undirectedWithoutCycle.getEdges());
        System.out.println("граф имеет цикл " + result);
//        assertFalse("Cycle detection error.", result);
        assert (!result);

    }

    public static void init() throws Exception {
        final Graph.Vertex<Integer> cv1 = new Graph.Vertex<>(1);
        final Graph.Vertex<Integer> cv2 = new Graph.Vertex<>(2);
        final Graph.Vertex<Integer> cv3 = new Graph.Vertex<>(3);
        final Graph.Vertex<Integer> cv4 = new Graph.Vertex<>(4);
        final Graph.Vertex<Integer> cv5 = new Graph.Vertex<>(5);
        final Graph.Vertex<Integer> cv6 = new Graph.Vertex<>(6);
        verticies = Arrays.asList(cv1, cv2, cv3, cv4, cv5, cv6);

        final Graph.Edge<Integer> ce1_2 = new Graph.Edge<>(1, cv1, cv2);
        final Graph.Edge<Integer> ce2_4 = new Graph.Edge<>(2, cv2, cv4);
        final Graph.Edge<Integer> ce4_3 = new Graph.Edge<>(3, cv4, cv3);
        final Graph.Edge<Integer> ce3_6 = new Graph.Edge<>(4, cv3, cv6);
        final Graph.Edge<Integer> ce5_6 = new Graph.Edge<>(5, cv5, cv6);
        final Graph.Edge<Integer> ce4_5 = new Graph.Edge<>(6, cv4, cv5);
        edges = Arrays.asList(ce1_2, ce2_4, ce4_3, ce3_6, ce5_6, ce4_5);

        expectedResult = Arrays.asList(cv6, cv3, cv5, cv4, cv2, cv1);
    }


    public static void topologicalSortOnDirectedGraph() {
        final Graph<Integer> digraph = new Graph<>(Graph.TYPE.DIRECTED, verticies, edges);
        System.out.println("input data " + digraph.getVertices());
        final List<Graph.Vertex<Integer>> results = TopologicalSort.sort(digraph);
//        assertTrue("Topological sort error. results=" + results, results.size() != 0);
        assert results != null;
        assert (results.size() != 0);
        System.out.println("result " + results);
        System.out.println("expectedResult " + expectedResult);
        compareResult(results, expectedResult);
    }

    private static void compareResult(List<Graph.Vertex<Integer>> results1, List<Graph.Vertex<Integer>> expectedResult) {
        final Iterator<Graph.Vertex<Integer>> iter1 = results1.iterator();
        final Iterator<Graph.Vertex<Integer>> iter2 = expectedResult.iterator();
//        assertTrue("Topological sort error. size1=" + results1.size() + " size2=" + expectedResult.size(), results1.size() == expectedResult.size());
        assert (results1.size() == expectedResult.size());
        while (iter1.hasNext() && iter2.hasNext()) {
            final Graph.Vertex<Integer> v1 = iter1.next();
            final Graph.Vertex<Integer> v2 = iter2.next();
//            assertTrue("Topological sort error. v1=" + v1 + " v2=" + v2, v1.equals(v2));
            assert (v1.equals(v2));
        }
    }

}
